// Add module redis, prompt
//please if you have not module : npm install redis / npm install prompt
//If you have problem with prompt download on git the master branch and place it on node_modules
var redis = require('redis');
var prompt = require('prompt');

//Declaration of pseudo for user and messages
var pseudo = '';
var messages = '';

//Creation of client (default on 127.0.0.1 port 6379)
var subscriber = redis.createClient();
var publisher = redis.createClient();
prompt.start();

//error manage
subscriber.on("error", function (err) {
    console.log("Error " + err);
});
publisher.on("error", function (err) {
    console.log("Error " + err);
});

//Allow user to choose a pseudo
function choosePseudo(){
prompt.get(['pseudo'], function (err, result) {
	pseudo = result.pseudo;
	sendMessage();
});
}

//Allow user to send message on a channel
function sendMessage(){
prompt.get(['message'], function (err, result) {
	messages = result.message;
	publisher.publish("channel 1", pseudo + " send message :" + messages);
	sendMessage();
});
};

//Display message on connection of user
publisher.on('connect', function(channel,message){
    console.log('you are connected to the chat, please start to talk. Have fun with RibreauChat !');
    choosePseudo();
});

//Display message send by user
subscriber.on("message", function(channel, message) {
    console.log(message);   
});

//Subscrition to the channel
subscriber.subscribe("channel 1");
