# Prerequisites
- [NodeJS](https://nodejs.org/en/download/)
- [Redis (3.2.6)](http://redis.js.org)
- [Prompt](https://www.npmjs.com/package/prompt)

# Quickstart

## Install dependencies
- npm install redis
- npm install prompt

## Start the server
- redis-server

## Start client
- node chat.js

## TODO
- choose a pseudo
- start to talk